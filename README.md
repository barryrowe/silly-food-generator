# silly-food-generator

My friends and I were bullshitting silly foods, and after a while I had to make
a generator. This runs in the console with Node.

[See it on the web](http://ericlathrop.com/silly-food-generator/)

Examples:

```
whole-grain fermented veggie rangoons
original recipe shredded flamin' hot babies with chili-lime beans
Aunt Edna's shredded truffle boys with salted gravy
Daddy's warm sponge water
all-natural warm peppermint snacks
fat-free boneless aoli poppers
Grandma Edna's glazed savory gushers with buffalo crumbles
low-cal shredded chili-lime bars
imitation sauteed cream flakes
king size ghost bone danglers
malted aoli chili in natural casing
whole-grain chocolate-dipped chocolate rangoons with ketchup crumbles
Wisconsin crunchy ranch cakes
gluten-free fresh jello poppers by the foot
Louisiana chilled ranch gushers
extra crispy canned cheesey nuggets with savory gravy
original recipe smoked blue raspberry water on ciabatta
Louisiana Italian-style mustard dogs
traditional fermented french onion water with buffalo beans
open-faced chocolate-dipped sesame skins
mini warm cheesey gummies
whole-grain candied savory skins
king size glazed cheesey danglers on on the cob
fat-free peeled savory straws
light sous-vide tomato melts
fat-free crunch cheesey biscuits with fruit gravy
mini creamed sliders
open-faced twice-baked sour cakes
Mother's grilled buffalo buns
```
