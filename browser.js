import { makeFood } from "./make-food.js";

var foods = document.getElementById("foods");
var more = document.getElementById("more");

function generateFoods(number) {
  var foods = [];
  for (var i = 0; i < number; i++) {
    foods.push(makeFood());
  }
  return foods;
}

function displayFoods(number) {
  foods.innerHTML = generateFoods(number).map(food => `<p>${food}</p>`).join("");
}

displayFoods(10);

more.addEventListener("click", () => displayFoods(10));
